package config

import (
	"testing"
	"fmt"
	"reflect"
)

func TestType(t *testing.T){
	tst := map[string]interface{}{"string":""}
	tst2 := 10
	tst3 := 1.2
	tst4 := []interface{}{2}

	fmt.Println(getType(tst))
	fmt.Println(reflect.TypeOf(tst2))
	fmt.Println(reflect.TypeOf(tst3))
	fmt.Println(getType(tst4))
}

func getType(tst interface{}) reflect.Type {
	return reflect.TypeOf(tst)
}